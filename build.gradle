import edu.wpi.first.gradlerio.deploy.roborio.RoboRIO

plugins {
    id "java"
    id "edu.wpi.first.GradleRIO" version "2022.2.1"
    id "com.diffplug.spotless" version "5.9.0"
}

sourceCompatibility = JavaVersion.VERSION_11
targetCompatibility = JavaVersion.VERSION_11

def ROBOT_MAIN_CLASS = "com.chargerrobotics.Robot"

// Define my targets (RoboRIO) and artifacts (deployable files)
// This is added by GradleRIO's backing project DeployUtils.
deploy {
    targets {
        roborio(getTargetTypeClass('RoboRIO')) {
            // Team number is loaded either from the .wpilib/wpilib_preferences.json
            // or from command line. If not found an exception will be thrown.
            // You can use getTeamOrDefault(team) instead of getTeamNumber if you
            // want to store a team number in this file.
            team = project.frc.getTeamNumber()
            debug = project.frc.getDebugOrDefault(false)

            artifacts {
                // First part is artifact name, 2nd is artifact type
                // getTargetTypeClass is a shortcut to get the class type using a string

                frcJava(getArtifactTypeClass('FRCJavaArtifact')) {
                }

                // Static files artifact
                frcStaticFileDeploy(getArtifactTypeClass('FileTreeArtifact')) {
                    files = project.fileTree('src/main/deploy')
                    directory = '/home/lvuser/deploy'
                }
            }
        }
    }
}

def deployArtifact = deploy.targets.roborio.artifacts.frcJava

// Set to true to use debug for JNI.
wpi.java.debugJni = false

// Set this to true to enable desktop support.
def includeDesktopSupport = false

repositories {
    maven { url 'https://jitpack.io' }
}


// Defining my dependencies. In this case, WPILib (+ friends), and vendor libraries.
// Also defines JUnit 4.
dependencies {
    /*****************************************
     *       3786 specific dependencies      *
     *****************************************/
    implementation 'com.github.Carleslc:Simple-YAML:1.4.1'
    implementation 'com.fazecast:jSerialComm:2.5.3'
    implementation 'org.slf4j:slf4j-api:1.7.30'
    runtimeOnly 'ch.qos.logback:logback-classic:1.2.3'

    testImplementation 'org.testng:testng:6.9.10'
    testImplementation 'org.mockito:mockito-core:2.22.0'
    /*****************************************
     *     End 3786 specific dependencies    *
     *****************************************/

    implementation wpi.java.deps.wpilib()
    implementation wpi.java.vendor.java()

    roborioDebug wpi.java.deps.wpilibJniDebug(wpi.platforms.roborio)
    roborioDebug wpi.java.vendor.jniDebug(wpi.platforms.roborio)

    roborioRelease wpi.java.deps.wpilibJniRelease(wpi.platforms.roborio)
    roborioRelease wpi.java.vendor.jniRelease(wpi.platforms.roborio)

    nativeDebug wpi.java.deps.wpilibJniDebug(wpi.platforms.desktop)
    nativeDebug wpi.java.vendor.jniDebug(wpi.platforms.desktop)
    simulationDebug wpi.sim.enableDebug()

    nativeRelease wpi.java.deps.wpilibJniRelease(wpi.platforms.desktop)
    nativeRelease wpi.java.vendor.jniRelease(wpi.platforms.desktop)
    simulationRelease wpi.sim.enableRelease()

    testImplementation 'junit:junit:4.12'
}

def runUnitTestsOnly = project.hasProperty("runUnitTestsOnly")
def runFunctionalTestsOnly = project.hasProperty("runFunctionalTestsOnly")

test {
    useTestNG() {
        dependsOn cleanTest
        if (runUnitTestsOnly) {
            suites "src/test/resources/unit.xml"
        } else if (runFunctionalTestsOnly) {
            suites "src/test/resources/functional.xml"
        } else {
            suites "src/test/resources/functional.xml"
            suites "src/test/resources/unit.xml"
        }
    }
}

// Simulation configuration (e.g. environment variables).
wpi.sim.addGui().defaultEnabled = true
wpi.sim.addDriverstation()

// Setting up my Jar File. In this case, adding all libraries into the main jar ('fat jar')
// in order to make them all available at runtime. Also adding the manifest so WPILib
// knows where to look for our Robot Class.
jar {
    from { configurations.runtimeClasspath.collect { it.isDirectory() ? it : zipTree(it) } }
    manifest edu.wpi.first.gradlerio.GradleRIOPlugin.javaManifest(ROBOT_MAIN_CLASS)
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

// Configure jar and deploy tasks
deployArtifact.jarTask = jar
wpi.java.configureExecutableTasks(jar)
wpi.java.configureTestTasks(test)

// Setup spotless linting
spotless {
    // optional: limit format enforcement to just the files changed by this feature branch
    ratchetFrom 'origin/master'

    format 'misc', {
        // define the files to apply `misc` to
        target '*.gradle', '*.md', '.gitignore'

        // define the steps to apply to those files
        trimTrailingWhitespace()
        indentWithSpaces() // Takes an integer argument if you don't like 4
        endWithNewline()
    }
    java {
        // apply a specific flavor of google-java-format
        googleJavaFormat('1.8')

        trimTrailingWhitespace()
        endWithNewline()
        removeUnusedImports()
    }
}
